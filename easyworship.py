from pywinauto.application import Application
import win32clipboard

from schedule import ScheduleItemType


class EW:
    empty_song_template_name = "Leeg"
    new_empty_info_slide_text = "slide ({})"

    def __init__(self, app_path):
        self.app_path = app_path
        self.app = None
        self.main_window = None
        self.editor_window = None
        self.save_window = None
        self.database_tabs = None
        self.save_button = None
        self.schedule_list = None

        # Songs
        self.song_search_title_input = None
        self.song_search_add_button = None
        self.song_edit_title_input = None
        self.song_edit_content_textarea = None
        self.edit_song_save_button = None

        # Scriptures
        self.scripture_search_title_input = None
        self.scripture_search_add_button = None

        # Save dialog
        self.save_dialog_save_button = None
        self.save_dialog_file_name_input = None

    def start(self):
        print("Starting the application")
        self.app = Application().start(self.app_path)
        self.init_main_window()

    def connect(self):
        print("connecting to the application")
        self.app = Application().connect(path=self.app_path)
        self.init_main_window()

    def stop(self):
        print("Stopping the application")
        self.app.kill()

    #
    # Window inits
    #

    def init_main_window(self):
        self.main_window = self.app.TFormEZWorshipMain
        self.main_window.wait("ready")
        self.get_main_window_controls()

    def init_editor_window(self):
        self.editor_window = self.app.Editor
        self.get_editor_window_controls()

    def init_save_window(self):
        self.save_window = self.app.SaveScheduleAs
        self.get_save_window_controls()

    def get_main_window_controls(self):
        # Song controls
        self.song_search_add_button = self.main_window.AddSongtoSchedule

        songs_tab = next(
                c for c in self.main_window.children() if c.window_text() == "Songs" and c.class_name() == "TTabSheet")
        self.song_search_title_input = next(c for c in songs_tab.children() if c.class_name() == "ThvDBIncSearchCombo")

        # Scripture controls
        self.scripture_search_add_button = self.main_window.AddScripturetoSchedule  # is the same as self.song_search_add_button

        scripture_tab = next(c for c in self.main_window.children() if
                             c.window_text() == "Scriptures" and c.class_name() == "TTabSheet")
        self.scripture_search_title_input = next(
                c for c in scripture_tab.children() if c.class_name() == "TScriptureLocator")

        # Common controls
        self.database_tabs = songs_tab.parent()
        self.save_button = self.main_window.SaveSchedule
        self.schedule_list = next(c for c in self.song_search_add_button.parent().parent().parent().children() if
                                  c.class_name() == "TDBGridPlus")

    def get_editor_window_controls(self):
        self.edit_song_save_button = self.editor_window.OK
        self.song_edit_title_input = next(c for c in self.editor_window.children() if c.class_name() == "TEdit")
        self.song_edit_content_textarea = next(
                c for c in self.editor_window.children() if c.class_name() == "TSongSelector")

    def get_save_window_controls(self):
        self.save_dialog_save_button = self.save_window.Save
        self.save_dialog_file_name_input = next(c for c in self.save_window.children() if c.class_name() == "Edit")

    #
    # GUI interactions
    #

    def open_database_tab(self, index):
        # Reset tab index
        for i in range(0, 5):
            self.database_tabs.click()
            self.database_tabs.send_keystrokes('{VK_LEFT}')

        # Navigate to tab
        for i in range(0, index):
            self.database_tabs.click()
            self.database_tabs.send_keystrokes('{VK_RIGHT}')

    def open_song_database_tab(self):
        self.open_database_tab(0)

    def open_scripture_database_tab(self):
        self.open_database_tab(1)

    def add_song(self, item):
        print("Adding song: {}".format(item.database_title))
        self.open_song_database_tab()

        try:
            self.search_song_title(item.database_title)
            item.is_in_database = True
        except Exception as e:
            self.search_song_title(self.empty_song_template_name)
            item.is_in_database = False

        self.song_search_add_button.click()

    def search_song_title(self, title):
        self.song_search_title_input.double_click()
        self.song_search_title_input.send_chars(title)

        if self.song_search_title_input.window_text().strip().lower() != title.strip().lower():
            raise Exception("Song not found in database")

    def edit_selected_song(self, item, verses=None):
        self.schedule_list.send_keystrokes('{VK_DOWN}')
        self.main_window.menu_select("Schedule->0")
        self.init_editor_window()

        self.set_edit_song_title(item.display_title)

        if item.type == ScheduleItemType.SONG and verses is not None:
            self.set_edit_song_verses(verses)
        elif item.type == ScheduleItemType.INFO and not item.is_in_database:
            self.set_edit_song_content(self.new_empty_info_slide_text.format(item.display_title))

        self.edit_song_save_button.click()

    def set_edit_song_title(self, title):
        if self.song_edit_title_input.window_text() == title:
            return

        self.song_edit_title_input.click()
        self.song_edit_title_input.send_keystrokes('^a')
        self.song_edit_title_input.send_keystrokes(title)

    def set_edit_song_verses(self, verses):
        # Retrieve content
        self.song_edit_content_textarea.set_focus()
        self.song_edit_content_textarea.send_keystrokes('^a^c')
        content = self._get_clipboard_data()

        # Edit content
        content = self.extract_verses_from_content(content, verses)

        # Insert content
        self.set_edit_song_content(content)

    def set_edit_song_content(self, content):
        self._set_clipboard_data(content)
        self.song_edit_content_textarea.set_focus()
        self.song_edit_content_textarea.send_keystrokes('^a^v')

    def extract_verses_from_content(self, content, verses):
        if not content:
            return ""

        # todo...
        return content

    def _get_clipboard_data(self):
        win32clipboard.OpenClipboard()
        data = win32clipboard.GetClipboardData()
        win32clipboard.CloseClipboard()
        return data

    def _set_clipboard_data(self, data):
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardText(data)
        win32clipboard.CloseClipboard()

    def add_scripture(self, item):
        self.open_scripture_database_tab()
        print("Adding scripture: {}".format(item.display_title))

        if item.start_verse is None:
            search_string = "{} {}:1".format(item.book, item.chapter)
        elif item.end_verse is None:
            search_string = "{} {}:{}".format(item.book, item.chapter, item.start_verse)
        else:
            search_string = "{} {}:{}-{}".format(item.book, item.chapter, item.start_verse, item.end_verse)

        self.scripture_search_title_input.click()
        self.scripture_search_title_input.send_chars(search_string)

        if item.start_verse is None:
            for i in range(0, 100):
                self.scripture_search_title_input.send_keystrokes('{VK_SHIFT down}{VK_DOWN}{VK_SHIFT up}')

        self.scripture_search_add_button.click()

    def save_schedule(self, filename):
        print("Saving the schedule as {}".format(filename))
        self.save_button.click()
        self.init_save_window()

        self.save_dialog_file_name_input.send_keystrokes(filename)
        self.save_dialog_save_button.click()
