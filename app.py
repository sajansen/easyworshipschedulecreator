from easyworship import EW

from schedule import ScheduleItem, Schedule, ScheduleItemType


def get_schedule_lines(file_path):
    # raw = parser.from_file(file_path)
    # lines = raw['content'].splitlines()
    text = """Welkom en mededelingen
Zingen: Gezang 303: 1, 4, 5 lb
Zingen (onder collecte): Opwekking 789 lopen op het water
Zegen
Zingen: amen"""
    lines = text.splitlines()
    lines = [line.strip() for line in lines if line.strip()]
    # lines = [ScheduleItem.filter_title(line) for line in lines]
    # lines = [line for line in lines if line]
    return lines


def import_schedule(file_path):
    print("Loading schedule")
    schedule = Schedule()
    for line in get_schedule_lines(file_path):
        schedule_item = ScheduleItem(line)
        schedule.items.append(schedule_item)

    return schedule


def main():
    schedule = import_schedule("./files/schedule1.pdf")

    ew = EW("D:\Programs\Softouch\EasyWorship\EasyWorship.exe")
    ew.start()
    ew.open_database_tab(0)

    for item in schedule.items:
        if not item.display_title:
            continue

        if item.type == ScheduleItemType.SONG:
            ew.add_song(item)

            if item.is_in_database and item.display_title == item.database_title and item.verses is None:
                continue
            ew.edit_selected_song(item)
            continue

        if item.type == ScheduleItemType.INFO:
            ew.add_song(item)

            # Check if we must edit the song, to save time
            if item.is_in_database and item.display_title == item.database_title:
                continue
            ew.edit_selected_song(item)
            continue

        if item.type == ScheduleItemType.SCRIPTURE:
            ew.add_scripture(item)
            continue

        raise Exception("Unknown ScheduleItemType")

    ew.save_schedule("schedule1")
    ew.stop()
    print("Done.")


if __name__ == "__main__":
    main()
