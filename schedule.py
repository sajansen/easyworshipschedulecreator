import re
from enum import Enum


class ScheduleItemType(Enum):
    SONG = 1
    SCRIPTURE = 2
    INFO = 3


class ScheduleItem:
    def __init__(self, title):
        self.title = title
        self.display_title = self.filter_title(self.title)
        self.database_title = None
        self.is_in_database = False
        self.type = None

        # Song properties
        self.verses = None

        # Scripture properties
        self.book = None
        self.chapter = None
        self.start_verse = None
        self.end_verse = None

        self.type = self.get_type_based_on_title(title)

        if self.type is ScheduleItemType.SONG:
            self.get_song_verses()
        elif self.type is ScheduleItemType.SCRIPTURE:
            self.get_scripture_elements()

        self.set_database_title()

    @staticmethod
    def filter_title(title):
        title = re.sub(r"\(.*\)", "", title, flags=re.IGNORECASE)

        title = re.sub(r"^(schrift)*lezing:", "", title, flags=re.IGNORECASE)

        title = re.sub(r"^zingen *:", "", title, flags=re.IGNORECASE)

        title = title.strip()

        title = re.sub(r"(Opwekking \d{1,3} ).*$", r"\1", title, flags=re.IGNORECASE)

        title = re.sub(r"^amen$", "", title, flags=re.IGNORECASE)

        title = re.sub(r"^collecte.*$", "Inzameling van de gaven", title, flags=re.IGNORECASE)

        title = re.sub(r"^stilgebed$", "Stil gebed", title, flags=re.IGNORECASE)

        title = title.strip()
        return title

    @staticmethod
    def get_type_based_on_title(title):
        if re.match(r"^.*lezing *:", title, re.IGNORECASE) or re.match(r"^lezen *:", title, re.IGNORECASE):
            return ScheduleItemType.SCRIPTURE

        if re.match(r"^zingen *:", title, re.IGNORECASE):
            return ScheduleItemType.SONG

        return ScheduleItemType.INFO

    def set_database_title(self):
        self.database_title = self.display_title

        if self.type == ScheduleItemType.INFO:
            return

        if self.type == ScheduleItemType.SCRIPTURE:
            return

        if self.type == ScheduleItemType.SONG:
            title = re.search(r"^([a-z\s]* *\d*):?", self.display_title, re.IGNORECASE)
            song_number = re.search(r"^[a-z\s]* *(\d*):?", self.display_title, re.IGNORECASE)
            post_fix = re.search(r"\d ([a-z]*)$", self.display_title, re.IGNORECASE)

            if not title:
                return
            title = title.group(1).strip()

            if not song_number:
                song_number = ""
            else:
                song_number = song_number.group(1).strip()
                if song_number:
                    song_number = int(song_number)
                    song_number = "{:03d}".format(song_number)
                    if "NLB" in title:
                        song_number = "0" + song_number

            if not post_fix:
                post_fix = ""
            else:
                post_fix = post_fix.group(1).strip()

            self.database_title = title
            if post_fix:
                if post_fix.lower() == "lb" and song_number:
                    self.database_title = "Liedboek {}".format(song_number)
                    self.display_title = self.database_title
                    if self.verses:
                        self.display_title += ": {}".format(', '.join(self.verses))
                    return
                self.database_title += " " + post_fix

    def __repr__(self):
        return "[{}] {}".format(self.type, self.title)

    def get_scripture_elements(self):
        search_result = re.search(r"^(\d? *[a-z]+) ?(\d+)(: ?(\d+)(-(\d+))?)?$", self.display_title, re.IGNORECASE)
        self.book = search_result.group(1)
        self.chapter = search_result.group(2)
        self.start_verse = search_result.group(4)
        self.end_verse = search_result.group(6)

    def get_song_verses(self):
        verses = []

        title = self.display_title
        if ":" not in title:
            self.verses = None
            return

        title = re.sub(r"^.*:", "", title, re.IGNORECASE)
        title = re.sub(r"[a-z]*$", "", title, re.IGNORECASE).strip()

        for s in title.split(','):
            s = s.strip()
            if not s:
                continue

            search_result = re.search(r"(\d+)-(\d+)", title, re.IGNORECASE)
            if search_result:
                for i in range(int(search_result.group(1)), int(search_result.group(2)) + 1):
                    verses.append(i)
                continue

            if " " in s:
                s.split(" ")
                for c in s:
                    if c.isdigit():
                        s = c
                        break

            if not s.isdigit():
                continue

            verses.append(s)

        self.verses = verses if verses else None


class Schedule:
    def __init__(self):
        self.items = []
